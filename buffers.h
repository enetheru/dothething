#ifndef BUFFERS_H
#define BUFFERS_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QMap>
#include <QPair>
#include <QCheckBox>

// buffer class
class Buffer : public QWidget
{
    Q_OBJECT

public:
    explicit Buffer( QWidget *parent = 0 );
    void setText( const QString & );
    QString getText();
    QString getLine( const int & );
    int getRows();

signals:
    void textChanged();

private slots:

private:
    QCheckBox *oufl; //only use first line
    // at the moment this uses plain text, but perhaps in the future it might
    // make sense to use the QTextEdit class for formatted text. eg for scripts
    QPlainTextEdit *edit;
    //TODO make some buffers read only, like PWD, etc... or make a separate
    //section for environment variables.
};

// buffer container
class Buffers : public QWidget
{
    Q_OBJECT

public:
    explicit Buffers( QWidget *parent = 0 );
    Buffer *operator[]( const QString &name );
    bool contains( const QString & );
    QString getBufferText( const QString &name );
    QString getBufferLine( const QString &name, const int &line );
    void setBuffer( const QString &name, const QString &content );
    void delBuffer( const QString &name );
    int getRows();

signals:
    void textChanged();

private slots:

private:
    QHBoxLayout *buttons_hbox;
    QHBoxLayout *buffers_hbox;
    QMap< QString, QPushButton *> buttons;
    QMap< QString, Buffer * > buffers;
};


#endif // BUFFERS_H
