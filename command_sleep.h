#ifndef COMMAND_SLEEP_H
#define COMMAND_SLEEP_H

#include "command.h"
#include <QWidget>
#include <QSpinBox>

class CommandSleep : public Command
{
    Q_OBJECT

public:
    explicit CommandSleep(QWidget *parent = 0);

private slots:
    void slotCompose();

private:
    QSpinBox *seconds;
};

#endif // COMMAND_SLEEP_H
