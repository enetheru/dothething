#ifndef TASK_H
#define TASK_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>

class Task : public QWidget
{
    Q_OBJECT

public:
    explicit Task( QWidget *parent = 0 );
    void setName( QString );

signals:
    void deleteMe( Task * );

private slots:
    void slotStatusStarted();
    void slotStatusStopped();
    void slotRemove();

private:
    QLabel *name;
    QLabel *status;
    QPushButton *stop;
    QPushButton *playpause;
    QPushButton *removebtn;
};

#endif // TASK_H
