#ifndef COMMAND_CUSTOM_H
#define COMMAND_CUSTOM_H

#include "command.h"
#include <QWidget>
#include <QLineEdit>
#include <QCheckBox>

class CommandCustom : public Command
{
    Q_OBJECT

public:
    explicit CommandCustom(QWidget *parent = 0);

private slots:
    void slotCompose();

private:
    QCheckBox *terminal;
    QLineEdit *command;
};

#endif // COMMAND_CUSTOM_H
