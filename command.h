#ifndef COMMAND_H
#define COMMAND_H

#include <QWidget>

class Command : public QWidget
{
    Q_OBJECT

public:
    explicit Command(QWidget *parent = 0) : QWidget( parent ) {};
    QString text() { return _text; }
    QString _text;

signals:
    void changed( QString );

private:
};

#endif // COMMAND_H
