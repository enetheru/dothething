#include "buffers.h"

#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QPair>
#include <QDebug>
#include <QCheckBox>

// Buffer
// ======
Buffer::Buffer( QWidget *parent )
    : QWidget( parent )
{
    oufl = new QCheckBox( "Only use first line?", this );
    edit = new QPlainTextEdit( this );
    connect( oufl, SIGNAL( stateChanged( int ) ),
             this, SIGNAL( textChanged() ) );
    connect( edit, SIGNAL( textChanged() ),
             this, SIGNAL( textChanged() ) );

    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->addWidget( oufl );
    vbox->addWidget( edit );
    setLayout( vbox );
}

void
Buffer::setText( const QString &input )
{
    edit->setPlainText( input );
}

QString
Buffer::getLine( const int &line )
{
    if( oufl->isChecked() ) return edit->toPlainText().section( "\n", 0, 0 );
    return edit->toPlainText().section( "\n", line, line );
}

QString
Buffer::getText()
{
    return edit->toPlainText();
}

int
Buffer::getRows()
{
    if( oufl->isChecked() ) return 1;
    return edit->toPlainText().count( '\n' ) + 1;
}

// Buffers
// =======
Buffers::Buffers( QWidget *parent )
    : QWidget(parent)
{
    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->addLayout( (buttons_hbox = new QHBoxLayout()) );
    vbox->addLayout( (buffers_hbox = new QHBoxLayout()) );
    this->setLayout( vbox );
}


Buffer *
Buffers::operator[]( const QString &name )
{
    if( buffers.contains( name ) ) return buffers[ name ];
    else return nullptr;
}

bool
Buffers::contains( const QString &name )
{
    return buffers.contains( name );
}

int
Buffers::getRows()
{
    int rows = 0;
    for( auto &buffer : buffers.values() ){
        rows = std::max( rows, buffer->getRows() );
    }
    return rows;
}

QString
Buffers::getBufferText( const QString &name )
{
    if(! buffers.contains( name ) ){
        return QString( "" );
    }
    return buffers[ name ]->getText();
}

QString
Buffers::getBufferLine( const QString &name, const int &line )
{
    if(! buffers.contains( name ) ){
        return QString( "" );
    }
    return buffers[ name ]->getLine( line );
}

void
Buffers::setBuffer( const QString &name, const QString &content )
{
    if(! buffers.contains( "name" ) ){
        QPushButton *button = new QPushButton( name, this );
        button->setCheckable( true );

        Buffer *buffer = new Buffer( this );
        buffer->setText( content );
        buffer->hide();

        connect( buffer, SIGNAL( textChanged() ),
                 this,   SIGNAL( textChanged() ) );
        connect( button, SIGNAL( clicked( bool ) ),
                 buffer, SLOT( setVisible( bool ) ) );

        buttons.insert( name, button );
        buffers.insert( name, buffer );

        buttons_hbox->addWidget( button );
        buffers_hbox->addWidget( buffer );
    }
    else {
        auto item = buffers[ name ];
        item->setText( content );
    }
    return;
}

void
Buffers::delBuffer( const QString &name )
{
    qDebug() << "FIXME Buffers::delbuffer( " << name;
    return;
}
