#include "command_sleep.h"

#include <QHBoxLayout>
#include <QSpinBox>
#include <QString>

CommandSleep::CommandSleep(QWidget *parent) :
    Command(parent)
{
    seconds = new QSpinBox( this );

    connect( seconds, SIGNAL( valueChanged( int ) ),
             this, SLOT( slotCompose() ) );

    slotCompose();
}

void
CommandSleep::slotCompose()
{
    _text = "sleep " + QString::number( seconds->value() );
    emit changed( _text );
}
