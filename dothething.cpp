#include "dothething.h"
#include "command_sleep.h"
#include "command_custom.h"
#include "task.h"
#include "buffers.h"

#include <QApplication>
#include <QPushButton>
#include <QProcess>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QString>
#include <iostream>
#include <QDebug>
#include <QRegularExpression>
#include <QClipboard>
#include <QLabel>
#include <QGroupBox>
#include <QSpacerItem>

DoTheThing::DoTheThing(QWidget *parent) :
    QWidget(parent)
{
    // Create and configure Widgets
    // ============================
    //setup command line
    toggleCompose = new QPushButton( "compose", this );
    toggleCompose->setCheckable( true );
    concurrent = new QCheckBox( "concurrent", this );
    fin = new QLineEdit( this );
    fin->setReadOnly( true );
    execute = new QPushButton( "Make it so", this );

    //setup buffers
    clipboard = QApplication::clipboard();
    buffers = new Buffers( this );
    buffers->setBuffer( "$CLIPBOARD", clipboard->text() );
    buffers->setBuffer( "$PWD", getenv( "PWD" ) );

    //setup command composer
    commandSelect = new QComboBox( this );
    commandSelect->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
    commandSelect->addItem( "sleep" );
    commandSelect->addItem( "custom" );

    //setup status line
    toggleTasks = new QPushButton( "Tasks", this );
    toggleTasks->setCheckable( true );
    toggleTasks->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
    status = new QLabel( "status blah de blah blah FIXME", this );

    //status list
    tasks = new QGroupBox( "tasks", this );
    tasks_vbox = new QVBoxLayout();
    tasks->setLayout( tasks_vbox );

    // connect up signals and slots
    // ============================
    connect( toggleCompose, SIGNAL( clicked( bool )                        ),
             this,            SLOT( slotViewCompose( bool )                ) );

    connect( toggleTasks,   SIGNAL( clicked( bool )                        ),
             tasks,           SLOT( setVisible( bool )                     ) );

    connect( buffers,       SIGNAL( textChanged()                          ),
             this,            SLOT( slotUpdate()                          ) );

    connect( commandSelect, SIGNAL( activated( QString )                   ),
             this,            SLOT( slotChangeCommand( QString )           ) );

    connect( execute,       SIGNAL( clicked()                              ),
             this,            SLOT( slotExecute()                          ) );

    // set the layout for the interface
    // ================================
    QVBoxLayout *vbox = new QVBoxLayout();
    QHBoxLayout *hbox;
    //command line
    vbox->addLayout( (hbox = new QHBoxLayout()) );
    hbox->addWidget( toggleCompose );
    hbox->addWidget( concurrent );
    hbox->addWidget( fin );
    hbox->addWidget( execute );

    //buffers
    vbox->addWidget( buffers );

    // command composer line
    comp_box = new QHBoxLayout();
    vbox->addLayout( comp_box );
    comp_box->addWidget( commandSelect );

    // status/process line FIXME turn into a widget
    vbox->addLayout( (hbox = new QHBoxLayout()) );
    hbox->addWidget( toggleTasks );
    hbox->addWidget( status );

    vbox->addWidget( tasks );
    vbox->addStretch( 0 );
    this->setLayout( vbox );

    //Run default slots
    //==================
    slotChangeCommand( "sleep" );
    slotViewDefault();
}

QString
DoTheThing::compose( const int &line )
{
    // FIXME this smells bad
    QString result = command->text();
    QRegularExpression re( "\\$[0-9A-Za-z]+" );
    auto matches = re.globalMatch( command->text() );
    // build new string from the pieces
    while( matches.hasNext() ){
        auto match = matches.next();
        // if no buffers exist with this name, remove it
        if(! buffers->contains( match.captured( 0 ) ) ){
            int j = 0;
            while( (j = result.indexOf( match.captured( 0 ) , j )) != -1){
                result.replace( j, match.captured( 0 ).size(), "" );
                ++j;
            }
            continue;
        }
        // if we are building the visual command, replace only if its a single line
        if( line == -1 && (*buffers)[ match.captured( 0 ) ]->getRows() > 1 ) continue;
        // else replace all instances of match with the line
        int j = 0;
        while( (j = result.indexOf( match.captured( 0 ) , j )) != -1){
             result.replace( j, match.captured( 0 ).size(),
                     buffers->getBufferLine( match.captured( 0 ), line ) );
            ++j;
        }
    }
    return result;
}

//FIXME I am unhappy that this function exists along side the
//slotChangeCommand, surely it should be one or the other, or at least have the
//slot call this one. gah my ignorance about best practices hurts
void
DoTheThing::changeCommand( const QString &command )
{
    this->slotChangeCommand( command );
}

void
DoTheThing::slotUpdate()
{
    fin->setText( compose( -1 ) );
}

void
DoTheThing::slotChangeCommand( QString which )
{
//FIXME, all the c style casting here betrayes an underlying problem with how
//the code is constructed. there is a clean way to do this, but i'm unsure at
//the moment how that is achieved. I have bigger fish to fry atm so this goes
//into code smell/technical debt
    if(! which.compare( "sleep" ) ){
        command.reset( (Command *)new CommandSleep( this ) );
    }
    else if(! which.compare( "custom" ) ){
        command.reset( (Command *)new CommandCustom( this ) );
    }

    comp_box->addWidget( command.get() );

    connect( command.get(), SIGNAL( changed( QString )                     ),
             this,            SLOT( slotUpdate()                          ) );

    slotUpdate();
}

void
DoTheThing::slotExecute()
{
    QRegularExpression re("\"([^\"]*)\"|'([^']*)'|[^\\s]+");
    for( int row = 0; row < buffers->getRows(); ++row )
    {
        qDebug() << compose( row );
        QString program;
        QStringList args;

        //split the final composition into the command and arguments.
        auto matches = re.globalMatch( compose( row ) );
        while( matches.hasNext() ){
            QRegularExpressionMatch match = matches.next();
            if(! match.captured(2).isNull() )
                args.append( match.captured(2) );
            else if(! match.captured(1).isNull() )
                args.append( match.captured(1) );
            else
                args.append( match.captured(0) );
        }
        if( args.isEmpty() ) return;
        else {
            program = args.first();
            args.removeFirst();
        }


        QProcess *myProcess = new QProcess( this );

        connect( myProcess, SIGNAL( started()                                  ),
                 this,        SLOT( slotIncrement()                            ) );

        connect( myProcess, SIGNAL( finished( int, QProcess::ExitStatus )      ),
                 this,        SLOT( slotDecrement()                            ) );

        myProcess->start( program, args );

        //adding status line
        Task *task = new Task( this );
        task->setName( program );

        connect( myProcess, SIGNAL( started()                                  ),
                 task,        SLOT( slotStatusStarted()                        ) );

        connect( myProcess, SIGNAL( finished( int, QProcess::ExitStatus )      ),
                 task,        SLOT( slotStatusStopped()                        ) );

        connect( task,      SIGNAL( deleteMe(Task *)                           ),
                 this,        SLOT( slotDeleteTask(Task *)                     ) );

        tasks_vbox->addWidget( task );
    }
}

void
DoTheThing::slotIncrement()
{
    processCount++;
    execute->setText( QString::number( processCount ) );
}

void
DoTheThing::slotDecrement()
{
    processCount--;
    execute->setText( QString::number( processCount ) );
    if( processCount == 0 ) execute->setText( "Make it so" );
}

void
DoTheThing::slotDeleteTask(Task *task)
{
    delete task;
}

void
DoTheThing::slotViewDefault()
{
    //FIXME hacky as fuck lol. remove this c cast
    for( auto child : this->children() ){
        if( child->isWidgetType() )((QWidget *)child)->hide();
    }
    toggleCompose->show();
    concurrent->show();
    toggleTasks->show();
    status->show();
    fin->show();
    execute->show();
}

void
DoTheThing::slotViewCompose( bool checked )
{
    if( checked ) {
        buffers->show();
        commandSelect->show();
        command->show();
    } else {
        buffers->hide();
        commandSelect->hide();
        command->hide();
    }
}
