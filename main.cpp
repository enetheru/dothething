#include "dothething.h"
#include <QApplication>
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName( "DoTheThing" );
    app.setApplicationVersion( "blaahhh" ); //FIXME this needs to be configured
    // by the build system.
    QCommandLineParser parser;

    parser.setApplicationDescription( "DoTheThing Description" ); //FIXME
    // placeholder text while testing
    // TODO add to the description programmatically the list of commands that
    // are currently implemented

    parser.addVersionOption();
    parser.addHelpOption();

    parser.addOptions({
            {{ "c", "command"}, "command to show by default", "command" },
            } );
    // TODO Additional options go here
    // * use a different configuration file
    // * process command immediately on starting
    // * quit after completing actions
    parser.process( app );

    DoTheThing w;
    if( parser.isSet( "command" ) ){
        //set the default command to display
        //TODO verify the validity of the string, and bail if not correct
        w.changeCommand( parser.value( "command" ) );
    }

    w.setWindowFlag( Qt::Dialog, true );
    w.show();

    return app.exec();
}
