#include "task.h"

#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>

Task::Task( QWidget *parent )
    : QWidget(parent)
{
    name = new QLabel( "name", this );
    status = new QLabel( "unknown", this );
    stop = new QPushButton( "stop", this );
    stop->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
    playpause = new QPushButton( "playpause", this );
    playpause->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );
    removebtn = new QPushButton( "X", this );
    removebtn->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed );

    connect( removebtn, SIGNAL ( clicked()    ), 
             this,        SLOT ( slotRemove() ) );

    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget( name );
    hbox->addWidget( playpause );
    hbox->addWidget( stop );
    hbox->addWidget( status );
    hbox->addWidget( removebtn );

    this->setLayout( hbox );
}

void
Task::slotStatusStarted() 
{
    status->setText( "started" );
}

void
Task::slotStatusStopped()
{
    status->setText( "stopped" );
}

void
Task::slotRemove() {
    emit deleteMe( this );
}

void
Task::setName( QString name )
{
    this->name->setText( name );
}
