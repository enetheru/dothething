Notes for little project to run commands
----------------------------------------

part of the problem i have with traditional file managers is the way that cut,
copy, paste commands are executed from within the application. things have
gotten a little better over the years with having the feedback windows separate
from the primary application but it still feels really clunky, and not very
robust

part of the way I want to make copying and pasting better is to have a sort of
command builder that you can specify options for, it will then run the command
as you desire.

for instance I want options like
* choice in verbosity
* concurrancy
* drop in replacements like rsync, xcopy
* completely different commands like diff
* context awareness like git rm and git mv
* job tracking and status
* history

basically all the things i enjoy from the command line, except packaged for my
file manager.

It should be as simple to use as CTRL+C and CTRL+V for the simple case, but
enable the user to perform more complex interactions.

Things it should be able to do eventually:
* Concurrancy
    * maximum job numbers
    * batching instead of line by line
    * scheduling
    * know the status of the child process and provide visual feedback
* Customizable commands via user scripts
    * Sensible defaults matching tool best practices
    * command profiles
* Optional input validation
* Discovery through tooltips and links to documentation
    * tooltips
    * links to man pages and online documentation
* display of the actual command it will run as copyable text
* realtime reporting and logs
    * time taken
    * stdout, stderr
    * history
* accepting and sending to pipes like normal cmd line arguments do?

NOTE: 99% of the time these features will be completely ignored, which is
exactly how it should be. its the 1% of the time you want to save people time
and energy.

Implemenation Plan
------------------
The first incarnation of the application wont require the file manager, and can
receive its input either manually or from the x system buffer.
Most file managers I have used copy their CTRL+C to the x buffer as plain text.
So we'll just use that.

OK so rough plan for implementation.
* [DONE] make a button that prints to stdout
* [DONE] make that button fork a process that runs a command. **QProcess**
* [DONE] take the xclipboard buffer as the first argument
    * [DONE] display and editing of the buffer
    * [PARTIAL] option to run concurrant, or sequential
* [PARTIAL]process tracking and logging
* devise a configuration file that allows defining commands seperately

scriptable commands
-------------------
having to recompile the program just to add another command seems dumb, so
scripting the commands would be preferable. and the commands themselves would
have sane defaults, but also have profiles which would apply in different
circumstances, so a command definition, then command profiles.

The command definition would specify the interface elements, the command
profiles would define the settings.

So far we have a custom command, sleep, and cp, now i want to add another one
'scripted' which builds its interface from an external script.

Concurrancy
--------------------------------
For non blocking operations, maximum bandwidth utilization, we would need to
graph exactly where the bottlenecks are or its kind of pointless, writing to an
io device isnt done in parallel so concurrency is stupid, but other jobs may
not be.

the primary application can write the pid of the process it spawns to a file
and use that to track the jobs for the current session. even if you kill the
parent if the current session pid's are saved then opening a new session can
check this file and resume checking them.

visual feedback
---------------
It might be worth creating little wrappers for
the generic programs so that it can be tracked possible with dbus being the
messaging protocol. some rudimentary status can be gleaned from the standard
QProcess spawning capabilities, but it wont give progress bars etc.

a visual implementation of standard programs might be useful for those who
need their visual feedback, either via cmd line, or qt, and hook into dbus for
feedback

or handing off to existing file management service

commands that make sense
------------------------
cp, mv, rm, rsync, diff, git

common actions
--------------
copy paste
cut paste
delete
recycle
send to

CTRL-C = CTRL-Command
---------------------
typical workflow for using the command module would be to CTRL+C to define the
list of files to take action on
then when it pops up the command list, change the options before pressing
CTRL+V to set the destination and complete, or just complete.
We could actually make this work on the system buffer to begin with.
or invoking it from the current directory, or invoking it on a list of files.

CTRL+C = invoke copy command on selection with default options
CTRL+X = invoke mv command on selection
CTRL+V = set destination and execute command

for non standard command, you could setup other hotkeys, or
CTRL+C, then change options becore CTRL+V

Invoking a second command resets the options for invoke.

there will be a filelist that is used for the invoke command, which should be
able to be filterable/modifyable etc.

Command tracking and stats
---------------------------------
What do i want to see
* play pause stop repeat
* link to command, input and output buffer, should buffers be user defined?
* time started, time elapsed, prediction if possible
* stdout and stderr
* status, running paused, failed, ...

all of this packaged up int a widget so it can be re-used over and over

but if i close the main app and re-start it then i dont want to lose my history
it will have to be kept on disk somewhere, which means setting a configuration
directory and keeping these things in files with preferences for deleting the
things

a system tray aplication could take advantage of this and keep track too, using
the same structures.

Taking a step back and looking at the higher level structure
------------------------------------------------------------
so this framework executes commands, and tracks their results. so if i was to
define a command i would need two things, the composer, the external process
and the status indicator.

it seems to me that when defining the commands i need to sort of group all
these things into a singular place. or have hooks to put the pieces in the
right spot. I'll have to think about it in detail later.

Selecting the command from the menu, populates tha area with the command
composer widget
* command composer
then we run the command which creates a process, but all the processes are the
same so there's no real special thing there, but we do track it as a separate
object for the signals and slots to fire.
then the status list is created with a status widget, but that status might
need special code from the command, using dbus etc. but that specialness might only be the status line itself. for simplicity's sake, were not going to bother wit any special things to start with.

argument variable replacement
-----------------------------
not all commands need arguments so the composer section will suck in the input
rather than the application. at the moment i just want to get the replacing
happening however it happens. so from my experimentation it needs to happen at
the compose level being populated into the final command. but the composer
doesnt know anything about the inputs, and maybe it shouldnt.. anyway, its at
the compose level that it should happen.

buffers
-------
Initially i thought that you would only need an input and an outut buffer to
draw from when constructing commands eg 'cp $1 $2' but once we thougt of using
a custom command then it opened up the buffers to be any number of variables,
and so i'm kind of stuck thinking that it should be generic and extensible.
with buffers named like $INPUT and $OUTPUT etc. and any arbitrary number.

the commands themselves would facilitate using the buffer.

now i want buffer functions like copy, rename, etc, hmm.

status line
-----------
I was thinking that the status line could be configurable, like a format string
that the user specified in a configuration file.

command composition
-------------------
What are the ways in which we can run the command, its either split the result
into multiple commands per line of the inputs, or run the command on the full
input, if it is multiple lines, then we should indicate that the command would
be run multiple times

[concurrant per line]
$1 = multiline\ninput
custom command = echo $1
view = echo $1
result = echo multiline
result = echo input

$1 = singleline
custom command = echo $1
view = echo singleline
result = echo singleline

[single command]
$1 = multiline\ninput
custom command = echo $1
result = echo multiline\ninput

which means we need a new tickbox

Congifuration
-------------
So what is configurable?
* default command
* command profiles
* hotkey for completion CTRL+V
* cmd line switch to change the config
