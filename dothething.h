#ifndef DOTHETHING_H
#define DOTHETHING_H

#include "command_custom.h"
#include "task.h"
#include "buffers.h"

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QCheckBox>
#include <QClipboard>
#include <QComboBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QGroupBox>
#include <memory>

class DoTheThing : public QWidget
{
    Q_OBJECT

public:
    explicit DoTheThing(QWidget *parent = 0);
    QString compose( const int & );
    void changeCommand( const QString & );

private slots:
    void slotExecute();
    void slotDecrement();
    void slotIncrement();
    void slotDeleteTask(Task *);
    void slotChangeCommand( QString );
    void slotUpdate();

    void slotViewDefault();
    void slotViewCompose( bool );

private:

    QClipboard *clipboard;
    Buffers *buffers;

    QComboBox *commandSelect;
    std::unique_ptr<Command> command;
    QHBoxLayout *comp_box;

    QPushButton *toggleCompose;
    QCheckBox *concurrent;
    QLineEdit *fin;
    QPushButton *execute;

    QPushButton *toggleTasks;
    QLabel *status;
    QGroupBox *tasks;
    QVBoxLayout *tasks_vbox;

    int processCount = 0;
};

#endif // DOTHETHING_H
