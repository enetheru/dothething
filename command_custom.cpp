#include "command_custom.h"

#include <QHBoxLayout>
#include <QLineEdit>
#include <QString>

CommandCustom::CommandCustom(QWidget *parent) :
    Command(parent)
{
    command = new QLineEdit( this );
    terminal = new QCheckBox( "Open In Terminal" );

    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget( terminal );
    hbox->addWidget( command );
    setLayout( hbox );

    connect( terminal, SIGNAL( stateChanged( int ) ),
             this, SLOT( slotCompose() ) );

    connect( command, SIGNAL( textChanged( QString ) ),
             this, SLOT( slotCompose() ) );

    slotCompose();
}

void
CommandCustom::slotCompose()
{
    if( terminal->isChecked() ) _text = "terminology -e ";
    else _text = "";
    _text += command->text();
    emit changed( _text );
}
